//
//  Double.swift
//  CryptoTracker
//
//  Created by Prawira Hadi Fitrajaya on 08/03/22.
//

import Foundation

extension Double {
    
    /// Converts a Doubel into currency with 2 decimal places
    ///  ```
    /// Convert 1234.56 to Rp1,234.56
    ///  ```
    private var currencyFormatter2: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = .currency
        formatter.locale = .current
        formatter.currencyCode = "idr"
        formatter.currencySymbol = "Rp"
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        return formatter
    }
    
    /// Converts a Doubel into currency as a String 2 decimal places
    ///  ```
    /// Convert 1234.56 to "Rp1,234.56"
    ///
    func asCurrencyWith2Decimals() -> String {
        let number = NSNumber(value: self)
        return currencyFormatter2.string(from: number) ?? "Rp0.00"
    }
    
    /// Converts a Doubel into currency with 2-6 decimal places
    ///  ```
    /// Convert 1234.56 to Rp1,234.56
    /// Convert 12.3456 to Rp12.3456
    /// Convert 0.123456 to Rp0.123456
    ///  ```
    private var currencyFormatter6: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = .currency
        formatter.locale = .current
        formatter.currencyCode = "idr"
        formatter.currencySymbol = "Rp"
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 6
        return formatter
    }
    
    /// Converts a Doubel into currency as a String 2-6 decimal places
    ///  ```
    /// Convert 1234.56 to "Rp1,234.56"
    /// Convert 12.3456 to "Rp12.3456"
    /// Convert 0.123456 to "Rp0.123456"
    ///
    func asCurrencyWith6Decimals() -> String {
        let number = NSNumber(value: self)
        return currencyFormatter6.string(from: number) ?? "Rp0.00"
    }
    
    /// Converts a Doubel into string representation
    ///  ```
    /// Convert 1.2345 to "1.23"
    ///
    func asNumberString() -> String {
        return String(format: "%.2f", self)
    }
    
    /// Converts a Doubel into string representation with percentage symbol
    ///  ```
    /// Convert 1.2345 to "1.23%"
    ///
    func asPercentString() -> String {
        return asNumberString() + "%"
    }
}
