//
//  CryptoTrackerApp.swift
//  CryptoTracker
//
//  Created by Prawira Hadi Fitrajaya on 08/03/22.
//

import SwiftUI

@main
struct CryptoTrackerApp: App {
    
    @StateObject private var vm = HomeViewModel()
    
    var body: some Scene {
        WindowGroup {
            NavigationView {
                HomeView()
                    .navigationBarHidden(true)
            }
            .environmentObject(vm)
        }
    }
}
